

$(document).ready(function () {
  $("ul").on("click", ".init", function () {
    $(this).closest("ul").children('li:not(.init)').toggle();
  });

  var allOptions = $("ul").children('li:not(.init)');
  $("ul").on("click", "li:not(.init)", function () {
    allOptions.removeClass('selected');
    $(this).addClass('selected');
    $("ul").children('.init').html($(this).html());
    allOptions.toggle();
  });

  hiddenAll();
  CalcTotal();

  $(document).on('change', "#select_modulo", function () {
    hiddenAll();
  })
  $(document).on('change', "#select_total", function () {
    CalcTotal();
  })
})

function hiddenAll() {
  var valor = $('#select_modulo').val();
  $('.addos').fadeOut(0, function () {
    $('#addos_' + valor).fadeIn(0)
  })
  CalcTotal();
}

function CalcTotal() {
  var total = $('#select_total').val();
  var valor = $('#select_modulo').val();
  var modulo = valor;
  var insert = '';
  var aqui = $("#mod_geral > .atomo_destinos")[0].outerHTML;

  for (var i = 0; i < total; i++) {
    insert = insert + aqui + '\n\n';
  }
  //$("#" + modulo)[0].innerHTML = insert;
  $("#mod_geral")[0].innerHTML = insert;
}
