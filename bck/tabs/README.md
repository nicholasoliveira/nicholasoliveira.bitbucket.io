#TudoAzulTabsClub
### Este é simples plugin Tab's JQuery que segue o padrão do cliente \o/

* Use o arquivo **tabs2019** para ver o funcionamento.
* Existem três formas de alterar os **textos, imagens e CTA** das **subtabs** de **Clube** são elas: **Dinâmica**, **Estática** ou **Estática/Dinâmica**.

---
### Estática
*Use como base os arquivos **html-content.html** ou **html-content.minify.html** e altere os textos e imagens pelo HTML.

```js
...
/*Tab principal*/
$("#minha-primeira-tabs").AzulAtomoTabs2019({'opened': 0});
/* Sub tab clube */
$("#outa-tabs").AzulAtomoTabs2019({'opened': 0});
/*Sticks (sliders) dentro das tabs*/
$(".atomoClubePlanosBeneficios").slick({dots:false,infinite:false,arrows:true,speed:300,slidesToShow:1,slidesToScroll:1,});$(".atomoClubePlanosDestinos").slick({dots:false,infinite:false,arrows:true,speed:300,slidesToShow:2,slidesToScroll:1,responsive:[{breakpoint:992,settings:{slidesToShow:1,touchThreshold:10,arrows:true}}]});
/*Atualiza os sticks após o evento show da tab*/
$(document).on("click",".atomo-2019-tabs-nav > a",function(){$(".atomoClubePlanosBeneficios").slick("refresh");$(".atomoClubePlanosDestinos").slick("refresh")});
...
```
---

### Dinâmica
*Use como base os arquivos **js-content.html** ou **js-content.minify.html** e altere os textos e imagens da Aba Club pelo JAVASCRIPT.

```js
...
$(document).ready(function () {
/* Tab principal */
$("#minha-primeira-tabs").AzulAtomoTabs2019({ 'opened': 0 });
/* Sub tab clube */
$("#outa-tabs").AzulAtomoTabs2019({
    'opened': 0,
    'dinamic_content': true,
    'promoarea': [{
        'index_tab': 4,
        'content_text': ['<span>Todo mês, <strong>20.000 pontos</strong> na sua conta.</span>', '<span>Com os 20.000 pontos de cada mês seguinte, <strong>ao final de um ano</strong>, você acumulou <strong>240.000 pontos</strong>.</span>'],
        'image_src': '/documents/26002/431585/graf-clube-20000.png',
        'image_src_mobile': '',
        'cta_text': 'Assine já',
        'cta_href': 'https://apps.voeazul.com.br/TudoAzulClub/index.html#!/Step1',
        'cta_class': 'atomoBtn atomoBtn atomo-texto-branco atomo-bg-azul_041e43',
    }],
});
/* Sticks (sliders) dentro das tabs */
$(".atomoClubePlanosBeneficios").slick({dots:false,infinite:false,arrows:true,speed:300,slidesToShow:1,slidesToScroll:1,});$(".atomoClubePlanosDestinos").slick({dots:false,infinite:false,arrows:true,speed:300,slidesToShow:2,slidesToScroll:1,responsive:[{breakpoint:992,settings:{slidesToShow:1,touchThreshold:10,arrows:true}}]});
/* Atualiza os sticks após o evento show da tab */
$(document).on("click",".atomo-2019-tabs-nav > a",function(){$(".atomoClubePlanosBeneficios").slick("refresh");$(".atomoClubePlanosDestinos").slick("refresh")});
...
```
---  

Nesse exemplo alteramos somente o conteúdo da aba Clube 20k por JAVASCRIPT, caso queira você poderá alterar o conteúdo de todas as abas: 
```js
...
    'dinamic_content': true,
    'promoarea': [{
        'index_tab': 3,
        'content_text': ['<span>Todo mês, <strong>10.000 pontos</strong> na sua conta.</span>', '<span>Com os 10.000 pontos de cada mês seguinte, <strong>ao final de um ano</strong>, você acumulou <strong>100.000 pontos</strong>.</span>'],
        'image_src': '/documents/26002/431585/graf-clube-10000.png',
        'image_src_mobile': '',
        'cta_text': 'Assine já',
        'cta_href': 'https://apps.voeazul.com.br/TudoAzulClub/index.html#!/Step1',
        'cta_class': 'atomoBtn atomoBtn atomo-texto-branco atomo-bg-azul_041e43',
    },
    {
        'index_tab': 4,
        'content_text': ['<span>Todo mês, <strong>20.000 pontos</strong> na sua conta.</span>', '<span>Com os 20.000 pontos de cada mês seguinte, <strong>ao final de um ano</strong>, você acumulou <strong>240.000 pontos</strong>.</span>'],
        'image_src': '/documents/26002/431585/graf-clube-20000.png',
        'image_src_mobile': '',
        'cta_text': 'Assine já',
        'cta_href': 'https://apps.voeazul.com.br/TudoAzulClub/index.html#!/Step1',
        'cta_class': 'atomoBtn atomoBtn atomo-texto-branco atomo-bg-azul_041e43',
    },],
```
---

#Estática/Dinâmica
Você também poderá mesclar as duas versões e alterar dinâmicamente somente um pequeno trecho do código.
Para isso use como modelo **html-content.html** ou **html-content.minify.html**.
Nesse exemplo vamos mudar apenas o conteúdo da lista na aba Clube 1000:

```js
$("#outa-tabs").AzulAtomoTabs2019({
    'opened': 0,
    'dinamic_content': true,
    'promoarea': [{
        'index_tab': 0,
        'content_text': ['<span>Meu novo conteúdo</span>', '<span>Outro item do meu novo conteúdo</span>'],
        
    }],
});
```
---

#Entendendo
Para começar a usar a capacidade dinâmica do plugin, primeiro defina **dinamic_content** como **true** nas configurações de chamada do plugin:
```js
...
    'dinamic_content': true,
...    
```
Agora basta preencher o vetor **promoarea** com os dados a serem exibidos:
```js
...
'promoarea': [{
    'index_tab': '',//número da aba (lembre-se de começar a conta pelo 0)
    'content_text': '',//Vetor simples com o conteúdo da lista ['um conteúdo', 'outro conteúdo']
    'image_src': '', //Imagem de destaque
    'image_src_mobile': '', //Imagem de destaque para ser exibida em dispositivos moveis
    'cta_text': 'Assine já', //Texto do botão
    'cta_href': 'https://apps.voeazul.com.br/TudoAzulClub/index.html#!/Step1', //Link do botão
    'cta_class': 'atomoBtn atomoBtn atomo-texto-branco atomo-bg-azul_041e43', //Classe CSS do botão
}]    
  
```   



