var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var gulpUtil = require('gulp-util');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');

gulp.task('build', function () {
  return gulp
    .src(['src/**/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('build/js'));
});


gulp.task('watch', function () {
  gulp.watch(["src/**/*.js"], ['build']).on('change', function (e) {
    console.log('Javascrip file ' + e.path + ' has been changed. Compiling.');
  });
});

var scssFiles = './src/scss/style.scss';
var cssDest = './build/css';
var sassDevOptions = {
  outputStyle: 'expanded'
}
var sassProdOptions = {
  outputStyle: 'compressed'
}

gulp.task('sassdev', function () {
  return gulp.src(scssFiles)
    .pipe(sass(sassDevOptions).on('error', sass.logError))
    .pipe(gulp.dest(cssDest));
});

gulp.task('sassprod', function () {
  return gulp.src(scssFiles)
    .pipe(sass(sassProdOptions).on('error', sass.logError))
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest(cssDest));
});

gulp.task('watch2', function () {
  gulp.watch(scssFiles, ['sassdev', 'sassprod']);
});

gulp.task('default', ['sassdev', 'sassprod', 'watch2', 'watch']);